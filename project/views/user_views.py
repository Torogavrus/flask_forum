from flask import render_template, flash, redirect, request, url_for
from flask_login import login_user, logout_user, login_required, current_user

from project import app, bcrypt
from project.service.user_service import UserService
from project.service.post_service import PostService
from project.forms.forms import LoginForm, RegisterForm, EditUser


@app.route('/member')
@login_required
def member():
    post_service = PostService()
    posts = post_service.find_posts_by_user(current_user)
    if not posts:
        flash('User ' + current_user.name + " hasn't any post.")
    return render_template('member.html', posts=posts)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user_service = UserService()
        user = user_service.find_user_by_email(form.email.data)
        if user and bcrypt.check_password_hash(user.password, request.form['password']):
            login_user(user)
            flash('You are logged in. Welcome!', 'success')
            return redirect(url_for(".index"))
        else:
            flash('Invalid email and/or password.', 'danger')
            return render_template('login.html', form=form)
    return render_template('login.html', form=form)

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if form.validate_on_submit():
        user_service = UserService()
        result = user_service.add_user(
            form.name.data,
            form.email.data,
            form.about_me.data,
            form.password.data
        )

        if isinstance(result, str):
            flash(result, 'error')

        else:
            flash('Thank you for registering.', 'success')
            login_user(result)
            return redirect(url_for(".member"))

    return render_template('register.html', form=form)

@app.route('/edit_user', methods=['GET', 'POST'])
@login_required
def edit_user():
    user = current_user
    form = EditUser(request.form)
    if form.validate_on_submit():
        user_service = UserService()
        result = user_service.update_user(user, form.name.data, form.about_me.data)
        if isinstance(result, str):
            flash(result)
        else:
            flash('Your changes have been saved.')
            return redirect(url_for(".member"))

    form.about_me.data = user.about_me
    return render_template('edit_user.html', form=form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You were logged out. Bye!', 'success')
    return redirect(url_for('index'))

@app.before_request
def before_request():
    user = current_user
    if not user.is_anonymous:
        user_service = UserService()
        user_service.update_last_seen(user)

# @app.route('/user/<name>')
# @login_required
# def user(name):
#     user_service = UserService()
#     user = user_service.find_user_by_name(name)
#     if user == None:
#         flash('User ' + name + ' not found.')
#         return redirect(url_for('index'))
#
#     post_service = Post_Service()
#     posts = post_service.find_posts_by_user(user)
#     if not posts:
#         flash('User ' + name + " hasn't any post.")
#
#     return render_template('user.html', user=user, posts=posts)