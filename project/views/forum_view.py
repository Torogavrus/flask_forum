from flask import render_template, flash, redirect, url_for, request
from flask_login import login_required, current_user

from project import app
from project.service.theme_service import ThemeService
from project.service.post_service import PostService
from project.service.answer_service import AnswerService
from project.forms.forms import AddPost, AddAnswer

@app.route('/')
@app.route('/index/')
@app.route('/forum/')
@app.route('/forum/<int:page>')
def index(page=1):
    theme_service = ThemeService()
    themes = theme_service.find_page_all(page, app.config.get('POSTS_PER_PAGE'))
    return render_template('index.html', themes=themes)

@app.route('/theme/<int:t_id>')
def theme(t_id, page=1):
    if request.args and 'page' in request.args:
        page = int(request.args['page'])
    theme_service = ThemeService()
    theme = theme_service.find_by_id(t_id)
    post_service = PostService()
    posts = post_service.find_post_by_theme_page(t_id, page, app.config.get('POSTS_PER_PAGE'))
    return render_template('theme.html', theme=theme, posts=posts)

@app.route('/add_post', methods=['GET', 'POST'])
@login_required
def add_post():
    theme_service = ThemeService()
    themes = theme_service.find_all()

    form = AddPost()
    form.theme.choices = [(x.id, x.name) for x in themes]

    if form.validate_on_submit():
        theme_service = ThemeService()
        theme = theme_service.find_by_id(form.theme.data)
        post_service = PostService()
        result = post_service.add_post(current_user, theme, form.title.data, form.body.data)
        if result:
            flash('post added', 'success')
            return redirect(url_for(".theme", t_id=form.theme.data))

    if request.args and 't_id' in request.args:
        form.theme.data = int(request.args['t_id'])

    return render_template('add_post.html', form=form)

@app.route('/post/<int:p_id>', methods=['GET', 'POST'])
def answers(p_id, page=1):
    if request.args and 'page' in request.args:
        page = int(request.args['page'])
    post_service = PostService()
    post = post_service.find_post_by_id(p_id)

    ansver_service = AnswerService()
    answers = ansver_service.find_by_post_page(post, page, app.config.get('POSTS_PER_PAGE'))

    form = AddAnswer()
    if form.validate_on_submit():
        result = ansver_service.add_answer(current_user, post, form.body.data)
        if result:
            flash('Answer added!', 'success')
            return redirect(url_for(".answers", p_id=p_id, page=page))

    return render_template('answer.html', post=post, answers=answers, form=form)
