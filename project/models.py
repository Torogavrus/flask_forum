from datetime import datetime
from project import app, db, bcrypt

class Roles(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True)

    def __init__(self, name):
        self.name = name

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False, unique=True)

    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'), nullable=False, default=1)
    role = db.relationship('Roles', backref=db.backref('user', lazy='dynamic'))

    email = db.Column(db.String(255), nullable=False, unique=True)
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime)
    password = db.Column(db.String(255), nullable=False)

    def __init__(self, name, email, password, role=None, about_me=None):
        self.name = name
        self.role = role
        self.email = email
        self.about_me = about_me
        self.password = bcrypt.generate_password_hash(
            password, app.config.get('BCRYPT_LOG_ROUNDS')
        ).decode('utf-8')

    def validate_password(self, password):
        return bcrypt.verify(password, self.password)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def __repr__(self):
        return "<User (username: {}; email: {}, role: {}, password: {})>".format(self.name, self.email, self.role.name, self.password)


class Theme(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    timestamp = db.Column(db.DateTime, nullable=False)

    def __init__(self, name, timestamp=None):
        self.name = name

        if not timestamp:
            timestamp = datetime.now()
        self.timestamp = timestamp

    def __repr__(self):
        return '<Theme: {}>'.format(self.name)


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128), nullable=False)
    timestamp = db.Column(db.DateTime, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User', backref=db.backref('post', lazy='dynamic'))
    theme_id = db.Column(db.Integer, db.ForeignKey('theme.id'), nullable=False)
    theme = db.relationship('Theme', backref=db.backref('post', lazy='dynamic'))

    def __init__(self, user, theme, title, timestamp=None):
        self.user = user
        self.theme = theme
        self.title = title

        if not timestamp:
            timestamp = datetime.now()
        self.timestamp = timestamp

    def __repr__(self):
        return '<Post {}>'.format(self.body)


class Answer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(10000), nullable=False)
    timestamp = db.Column(db.DateTime, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User', backref=db.backref('answer', lazy='dynamic'))
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'), nullable=False)
    post = db.relationship('Post', backref=db.backref('answer', lazy='dynamic'))

    def __init__(self, user, post, body, timestamp=None):
        self.user = user
        self.post = post
        self.body = body

        if not timestamp:
            timestamp = datetime.now()
        self.timestamp = timestamp

    def __repr__(self):
        return '<Post {}>'.format(self.body)
