import os
from flask import Flask
from flask_login import LoginManager
from flask_bcrypt import Bcrypt
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)

app_settings = os.getenv('APP_SETTINGS', 'project.config.DevelopmentConfig')
app.config.from_object(app_settings)

login_manager = LoginManager()
login_manager.init_app(app)
bcrypt = Bcrypt(app)

db = SQLAlchemy(app)

from project.models import User

login_manager.login_view = "login"
login_manager.login_message_category = 'danger'


@login_manager.user_loader
def load_user(user_id):
    return User.query.filter(User.id == int(user_id)).first()

import project.views.user_views
import project.views.forum_view
