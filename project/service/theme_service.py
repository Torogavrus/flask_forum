from project.models import Theme

class ThemeService():

    def find_all(self):
        return Theme.query.filter().all()

    def find_page_all(self, page_num, per_page):
        return Theme.query.filter().paginate(page_num, per_page, False)

    def find_by_id(self, t_id):
        return Theme.query.filter_by(id=t_id).first()
