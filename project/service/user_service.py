from datetime import datetime

from project import db
from project.models import User

class UserService():
    def find_user_by_name(self, name):
        return User.query.filter_by(name=name).first()

    def find_user_by_email(self, email):
        return User.query.filter_by(email=email).first()

    def add_user(self, name, email, about_me, password):
        user = User(
            name=name,
            email=email,
            about_me=about_me,
            password=password
        )

        name_exist = User.query.filter_by(name=name).first()
        if name_exist:
            return 'User with name {} already exist'.format(name)
        email_exist = User.query.filter_by(email=email).first()
        if email_exist:
            return 'User with email {} already exist'.format(email)
        db.session.add(user)
        db.session.commit()

        return user

    def update_user(self, user, name, about_me):
        need_change_flag = False
        if user.name != name:
            name_exist = User.query.filter_by(name=name).first()
            if name_exist:
                return 'User with name {} already exist'.format(name)
            user.name = name
            need_change_flag = True
        if user.about_me != about_me:
            user.about_me = about_me
            need_change_flag = True

        if need_change_flag:
            db.session.add(user)
            db.session.commit()
            return True
        else:
            return 'You changed nothing'

    def update_last_seen(self, user):
        user.last_seen = datetime.utcnow()
        db.session.add(user)
        db.session.commit()
