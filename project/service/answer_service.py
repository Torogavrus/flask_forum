from project import db
from project.models import Answer

class AnswerService():
    def find_by_post(self, post):
        return Answer.query.filter_by(post_id=post.id).order_by(Answer.timestamp).all()

    def find_by_post_page(self, post, page_num, per_page):
        return Answer.query.filter_by(post_id=post.id).order_by(Answer.timestamp).paginate(page_num, per_page, False)

    def add_answer(self, user, post, body):
        answer = Answer(
            user=user,
            post=post,
            body=body
        )

        db.session.add(answer)
        db.session.commit()

        return answer