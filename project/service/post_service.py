from project import db
from project.models import Post, Answer

class PostService():

    def find_post_by_id(self, p_id):
        return Post.query.filter_by(id=p_id).first()

    def find_posts_by_user(self, user):
        return Post.query.filter_by(user_id=user.id).all()

    def find_post_by_theme(self, t_id):
        return Post.query.filter_by(theme_id=t_id).all()

    def find_post_by_theme_page(self, t_id, page_num, per_page):
        return Post.query.filter_by(theme_id=t_id).paginate(page_num, per_page, False)

    def add_post(self, user, theme, title, body):
        post = Post(
            user=user,
            theme=theme,
            title=title
        )
        db.session.add(post)
        db.session.commit()

        answer = Answer(
            user=user,
            post=post,
            body=body
        )
        db.session.add(answer)
        db.session.commit()

        return post
