from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, TextAreaField, SelectField
from wtforms.validators import DataRequired, Email, Length, EqualTo

class LoginForm(FlaskForm):
    email = StringField(
        'email',
        validators=[
            DataRequired(),
            Email()
        ]
    )
    password= PasswordField(
        'password',
        validators=[DataRequired()]
    )


class RegisterForm(FlaskForm):
    name = StringField(
        'name',
        validators=[
            DataRequired(),
            Length(min=6, max=40)
        ]
    )

    email = StringField(
        'email',
        validators=[
            DataRequired(),
            Email(message=None),
            Length(min=6, max=40)
        ]
    )

    about_me = TextAreaField(
        'about_me',
        validators=[
            Length(min=0, max=140)
        ]
    )

    password = PasswordField(
        'password',
        validators=[DataRequired(), Length(min=6, max=25)]
    )

    confirm = PasswordField(
        'confirm',
        validators=[
            DataRequired(),
            EqualTo('password', message='Passwords must match.')
        ]
    )


class EditUser(FlaskForm):
    name = StringField(
        'name',
        validators = [
            DataRequired(),
            Length(min = 6, max = 40)
        ]
    )

    about_me = TextAreaField(
        'about_me',
        validators = [
            Length(min = 0, max = 140)
        ]
    )


class AddPost(FlaskForm):
    title = StringField(
        'title',
        validators = [
            DataRequired(),
            Length(min = 3, max = 40)
        ]
    )

    body = TextAreaField(
        'body',
        validators = [
            DataRequired(),
            Length(min = 0, max = 10000)
        ]
    )

    theme = SelectField('theme', coerce=int)


class AddAnswer(FlaskForm):
    body = TextAreaField(
        'body',
        validators=[
            DataRequired(),
            Length(min=0, max=10000)
        ]
    )
