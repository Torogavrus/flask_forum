from flask_script import Manager

from project import app, db
from project.models import Roles, User, Theme, Post, Answer

manager = Manager(app)

@manager.command
def create_tables():
    db.create_all()

@manager.command
def drop_tables():
    db.drop_all()

@manager.command
def fill_tables():
    r1 = Roles('user')
    r2 = Roles('admin')
    db.session.add(r1)
    db.session.add(r2)

    ua = User('admin', 'admin@email.com', 'admin', r2)
    u1 = User('user11', 'user11@email.com', 'user11')
    u2 = User('user22', 'user22@email.com', 'user22', about_me='well-known')
    u3 = User('user33', 'user33@email.com', 'user33', about_me='polite')
    db.session.add(ua)
    db.session.add(u1)
    db.session.add(u2)
    db.session.add(u3)

    t1 = Theme('Different')
    t2 = Theme('Advice me')
    t3 = Theme('Cars')
    t4 = Theme('Space ships')
    t5 = Theme('Bugs')
    t6 = Theme('Buterflys')
    db.session.add(t1)
    db.session.add(t2)
    db.session.add(t3)
    db.session.add(t4)
    db.session.add(t5)
    db.session.add(t6)

    p1 = Post(u1, t2, 'Help')
    p2 = Post(u2, t2, 'Rain')
    p3 = Post(u2, t3, 'Ford')
    p4 = Post(u1, t2, 'Weather')
    p5 = Post(u2, t2, 'Road')
    p6 = Post(u2, t2, 'House')
    p7 = Post(u2, t2, 'Table')
    db.session.add(p1)
    db.session.add(p2)
    db.session.add(p3)
    db.session.add(p4)
    db.session.add(p5)
    db.session.add(p6)
    db.session.add(p7)

    a1 = Answer(u1, p1, 'Test post #1')
    a2 = Answer(u2, p2, 'Rain is liquid water in the form of droplets')
    a3 = Answer(u2, p3, 'Test lololololo')
    a4 = Answer(u1, p4, 'rain rain rain')
    a5 = Answer(u2, p5, 'road trip. car')
    a6 = Answer(u2, p6, 'How to choose new hose&')
    a7 = Answer(u2, p7, 'How to choose new table?')
    a8 = Answer(u3, p2, 'The major cause of rain production is moisture moving along three-dimensional zones '
                        'of temperature and moisture contrasts known as weather fronts. If enough moisture and upward '
                        'motion is present, precipitation falls from convective clouds (those with strong upward '
                        'vertical motion) such as cumulonimbus (thunder clouds) which can organize into narrow '
                        'rainbands. In mountainous areas, heavy precipitation is possible where upslope flow '
                        'is maximized within windward sides of the terrain at elevation which forces moist air to '
                        'condense and fall out as rainfall along the sides of mountains. On the leeward side of '
                        'mountains, desert climates can exist due to the dry air caused by downslope flow which '
                        'causes heating and drying of the air mass. The movement of the monsoon trough, or '
                        'intertropical convergence zone, brings rainy seasons to savannah climes.')
    a9 = Answer(u2, p2, 'that have condensed from atmospheric water vapor and then precipitated')
    a10 = Answer(u3, p2, 'that is, become heavy enough to fall under gravity')
    a11 = Answer(u1, p2, 'Rain is a major component of the water cycle')
    a12 = Answer(u3, p2, 'and is responsible for depositing most of the fresh water')
    db.session.add(a1)
    db.session.add(a2)
    db.session.add(a3)
    db.session.add(a4)
    db.session.add(a5)
    db.session.add(a6)
    db.session.add(a7)
    db.session.add(a8)
    db.session.add(a9)
    db.session.add(a10)
    db.session.add(a11)
    db.session.add(a12)

    db.session.commit()

if __name__ == '__main__':
    manager.run()